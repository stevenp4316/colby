package com.runemate.game.events.osrs;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.events.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public class OSRSLobbyHandler extends GameEventHandler {
    private static final int OLD_LOBBY_WIDGET = 378;
    private static final int NEW_LOBBY_WIDGET = 413;

    public OSRSLobbyHandler() {
        super(PRIORITY_LOW);
    }

    private static InterfaceComponent getPlayButton() {
        var button = Interfaces.newQuery().containers(OLD_LOBBY_WIDGET, NEW_LOBBY_WIDGET)
            .grandchildren(false).types(InterfaceComponent.Type.SPRITE).sprites(429).results()
            .first();
        return button != null ? button : getBackupPlayButton();
    }

    private static InterfaceComponent getBackupPlayButton() {
        return Interfaces.newQuery().containers(OLD_LOBBY_WIDGET, NEW_LOBBY_WIDGET)
            .grandchildren(false).textContains("CLICK HERE").results()
            .first();
    }

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.LOBBY_HANDLER;
    }

    @Override
    public boolean isValid() {
        final InterfaceComponent button = getPlayButton();
        return button != null && button.isVisible();
    }

    @Override
    public void run() {
        final InterfaceComponent button = getPlayButton();
        if (button != null && button.isVisible() && button.click()) {
            Execution.delayUntil(() -> !button.isVisible(), 2500);
        }
    }

    @NonNull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }
}
