package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface SpotAnimationListener extends EventListener {

    default void onSpotAnimationSpawned(SpotAnimationSpawnEvent event) {
    }

}
