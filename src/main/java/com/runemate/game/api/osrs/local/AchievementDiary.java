package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.local.*;
import java.util.*;
import lombok.*;

//TODO Can this be deprecated with the Quest API update?
public enum AchievementDiary {
    ARDOUGNE(4458, 4459, 4460, 4461),
    DESERT(4483, 4484, 4485, 4486),
    FALADOR(4462, 4463, 4464, 4465),
    FREMENNIK(4491, 4492, 4493, 4494),
    KANDARIN(4475, 4476, 4477, 4478),
    KARAMJA(3578, 3599, 3611, 4566) {
        @Override
        public boolean isComplete(@NonNull Difficulty difficulty) {
            int varbit;
            switch (difficulty) {
                case EASY:
                    varbit = 3578;
                    break;
                case MEDIUM:
                    varbit = 3599;
                    break;
                case HARD:
                    varbit = 3611;
                    break;
                case ELITE:
                    varbit = 4566;
                    break;
                default:
                    varbit = -1;
                    break;
            }
            if (varbit != -1) {
                Varbit vbit = Varbits.load(varbit);
                if (vbit != null) {
                    if (Objects.equals(difficulty, Difficulty.ELITE)) {
                        return vbit.getValue() == 1;
                    }
                    return vbit.getValue() == 2;
                }
            }
            return false;
        }
    },
    KOUREND_AND_KEBOS(7925, 7926, 7927, 7928),
    LUMBRIDGE(4495, 4496, 4497, 4498),
    MORYTANIA(4487, 4488, 4489, 4490),
    VARROCK(4479, 4480, 4481, 4482),
    WESTERN_PROVINCES(4471, 4472, 4473, 4474),
    WILDERNESS(4466, 4467, 4468, 4469);
    private final int varbitEasy;
    private final int varbitMedium;
    private final int varbitHard;
    private final int varbitElite;

    AchievementDiary() {
        varbitEasy = -1;
        varbitMedium = -1;
        varbitHard = -1;
        varbitElite = -1;
    }

    AchievementDiary(int varbitEasy, int varbitMedium, int varbitHard, int varbitElite) {
        this.varbitEasy = varbitEasy;
        this.varbitMedium = varbitMedium;
        this.varbitHard = varbitHard;
        this.varbitElite = varbitElite;
    }

    public boolean isEasyComplete() {
        return isComplete(Difficulty.EASY);
    }

    public boolean isMediumComplete() {
        return isComplete(Difficulty.MEDIUM);
    }

    public boolean isHardComplete() {
        return isComplete(Difficulty.HARD);
    }

    public boolean isEliteComplete() {
        return isComplete(Difficulty.ELITE);
    }

    /**
     * Checks if this achievement diary is completed on the given difficulty.
     *
     * @param difficulty difficulty of the achievement diary.
     * @return true if complete, false otherwise
     */
    public boolean isComplete(@NonNull Difficulty difficulty) {
        int varbit;
        switch (difficulty) {
            case EASY:
                varbit = varbitEasy;
                break;
            case MEDIUM:
                varbit = varbitMedium;
                break;
            case HARD:
                varbit = varbitHard;
                break;
            case ELITE:
                varbit = varbitElite;
                break;
            default:
                varbit = -1;
                break;
        }
        if (varbit != -1) {
            Varbit vbit = Varbits.load(varbit);
            if (vbit != null) {
                return vbit.getValue() == 1;
            }
        }
        return false;
    }

    /**
     * Type representing the difficulties for each Achievement Diary.
     */
    public enum Difficulty {
        EASY,
        MEDIUM,
        HARD,
        ELITE
    }
}
