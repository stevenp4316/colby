package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class SpiritTreeVertex extends ObjectVertex implements SerializableVertex {

    private String text;

    public SpiritTreeVertex(Coordinate position, String text, Collection<WebRequirement> requirements) {
        super(position, (Pattern) null, (Pattern) null, requirements);
        this.text = text;
    }

    public SpiritTreeVertex(
        Coordinate position,
        Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements,
        int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public Pattern getTargetPattern() {
        if (this.target == null) {
            this.target = Regex.getPatternForExactStrings("Spirit tree", "Spirit Tree");
        }
        return target;
    }

    @Override
    public Pattern getActionPattern() {
        if (this.action == null) {
            this.action = Regex.getPatternForExactStrings("Travel", "Teleport");
        }
        return action;
    }


    @Override
    public int getOpcode() {
        return 14;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(text);
        return true;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean deserialize(int protocol, ObjectInput stream) {
        text = stream.readUTF();
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition())
            .append(getTargetPattern().pattern())
            .append(getActionPattern().pattern())
            .append(text)
            .toHashCode();
    }

    @Override
    public boolean step() {
        var component = getOSRSInterfaceComponent();
        if (component == null) {
            GameObject object = getObject();
            if (object != null && (object.isVisible() || Camera.turnTo(object)) && object.interact(action, target)) {
                Player avatar = Players.getLocal();
                if (avatar != null && !Execution.delayUntil(() -> getOSRSInterfaceComponent() != null, avatar::isMoving, 1200, 2400)) {
                    return false;
                }
                component = getOSRSInterfaceComponent();
            }
        }
        if (component != null) {
            int plane = Region.getCurrentPlane();
            Coordinate base = Region.getBase(plane);
            if (base != null && (component.interact("Continue") || component.click())) {
                return Execution.delayWhile(() -> base.equals(Region.getBase(plane)), 4200, 6000);
            }
        }
        return false;
    }

    @Override
    public GameObject getObject() {
        final GameObjectQueryBuilder builder = GameObjects.newQuery().within(new Area.Circular(getPosition(), 5));
        if (target != null) {
            builder.names(target);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (type != null) {
            builder.types(type);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        LocatableEntityQueryResults<GameObject> results = builder.results();
        if (results.size() > 1) {
            System.err.println("[ObjectVertex] " + results.size() + " results when querying " + this);
        }
        GameObject object = results.first();
        if (type == null && object != null) {
            type = object.getType();
        }
        return object;
    }

    private InterfaceComponent getOSRSInterfaceComponent() {
        return Interfaces.newQuery()
            .containers(187)
            .types(InterfaceComponent.Type.LABEL)
            .heights(16)
            .widths(386)
            .textContains(text)
            .visible()
            .results()
            .first();
    }

    @Override
    public String toString() {
        return "SpiritTreeVertex(name=" + getTargetPattern().pattern()
            + ", action=" + getActionPattern().pattern()
            + ", destination=" + text
            + ')';
    }
}
