package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import org.jetbrains.annotations.*;

/**
 * An arrow over the minimap pointing towards a destination
 */
public interface HintArrow extends Locatable, Renderable {
    int getType();

    @Nullable
    Actor getTarget();
}
