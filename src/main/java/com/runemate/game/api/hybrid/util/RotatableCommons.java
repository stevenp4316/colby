package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;

public final class RotatableCommons {
    public static final int NORTH = 180,
        SOUTH = 0,
        EAST = 270,
        WEST = 90,
        NORTH_EAST = 225,
        NORTH_WEST = 135,
        SOUTH_EAST = 315,
        SOUTH_WEST = 45;

    private RotatableCommons() {
    }

    public static boolean isFacing(Locatable camera, Locatable pointOfView) {
        if (camera instanceof Rotatable && pointOfView != null) {
            final Area.Rectangular cameraArea = camera.getArea();
            if (cameraArea != null) {
                final Area.Rectangular areaOfView = pointOfView.getArea();
                if (areaOfView != null) {
                    final int orientation = ((Rotatable) camera).getOrientationAsAngle();
                    for (Coordinate faceable : areaOfView.getCoordinates()) {
                        for (Coordinate lookingFrom : cameraArea.getCoordinates()) {
                            if ((
                                orientation == NORTH && lookingFrom.getX() == faceable.getX() &&
                                    lookingFrom.getY() < faceable.getY()
                            ) ||
                                (
                                    orientation == WEST && lookingFrom.getX() > faceable.getX() &&
                                        lookingFrom.getY() == faceable.getY()
                                ) ||
                                (
                                    orientation == SOUTH && lookingFrom.getX() == faceable.getX() &&
                                        lookingFrom.getY() > faceable.getY()
                                ) ||
                                (
                                    orientation == EAST && lookingFrom.getX() < faceable.getX() &&
                                        lookingFrom.getY() == faceable.getY()
                                ) ||
                                (
                                    orientation == SOUTH_WEST &&
                                        lookingFrom.getX() > faceable.getX() &&
                                        lookingFrom.getY() > faceable.getY()
                                ) ||
                                (
                                    orientation == SOUTH_EAST &&
                                        lookingFrom.getX() < faceable.getX() &&
                                        lookingFrom.getY() > faceable.getY()
                                ) ||
                                (
                                    orientation == NORTH_WEST &&
                                        lookingFrom.getX() > faceable.getX() &&
                                        lookingFrom.getY() < faceable.getY()
                                ) ||
                                (
                                    orientation == NORTH_EAST &&
                                        lookingFrom.getX() < faceable.getX() &&
                                        lookingFrom.getY() < faceable.getY()
                                )) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static int getYawOffset(int goal) {
        return getYawOffset(Camera.getYaw(), goal);
    }

    public static int getCorrectModuloOf(int amount, int modulo) {
        if (amount < 0) {
            return modulo - (Math.abs(amount) % modulo);
        }
        return amount % modulo;
    }

    public static int getYawOffset(int start, int goal) {
        return getCorrectModuloOf(((goal - start) + 180), 360) - 180;
    }
}