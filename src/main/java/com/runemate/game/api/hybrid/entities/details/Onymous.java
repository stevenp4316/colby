package com.runemate.game.api.hybrid.entities.details;

import org.jetbrains.annotations.*;

/**
 * Anything that has a name, the opposite of anonymous.
 */
@FunctionalInterface
public interface Onymous {
    @Nullable
    String getName();
}
