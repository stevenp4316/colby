package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;

public class OSRSCacheMaterial extends CacheMaterial {
    private final int id;
    private int hslColor;
    private boolean aBool1285;
    private int[] spriteIds;
    private int[] anIntArray1288;
    private int[] anIntArray1289;
    private int[] argbArray;
    private int type;
    private int mipmapLevel;
    private int[] pixels;

    public OSRSCacheMaterial(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Color getColor() {
        if (hslColor <= 0) {
            return null;
        }
        return new Color(
            RSColors.hsvToRgb(RSColors.hslToHsv(RSColors.smoothAnyHSLLighting(hslColor, 96))));
    }

    @Override
    public int getMipmapLevel() {
        return mipmapLevel;
    }

    @Override
    public int getEdgeLength() {
        //Always 128 unless in low memory mode
        return 128;
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        this.hslColor = stream.readUnsignedShort();
        this.aBool1285 = stream.readUnsignedByte() == 1;
        int spriteCount = stream.readUnsignedByte();
        if (spriteCount >= 1 && spriteCount <= 4) {
            this.spriteIds = new int[spriteCount];
            int index;
            for (index = 0; index < spriteCount; index++) {
                this.spriteIds[index] = stream.readUnsignedShort();
            }
            if (spriteCount > 1) {
                this.anIntArray1288 = new int[spriteCount - 1];
                for (index = 0; index < spriteCount - 1; index++) {
                    this.anIntArray1288[index] = stream.readUnsignedByte();
                }
            }
            if (spriteCount > 1) {
                this.anIntArray1289 = new int[spriteCount - 1];
                for (index = 0; index < spriteCount - 1; index++) {
                    this.anIntArray1289[index] = stream.readUnsignedByte();
                }
            }
            this.argbArray = new int[spriteCount];
            for (index = 0; index < spriteCount; index++) {
                this.argbArray[index] = stream.readInt();
            }
            this.type = stream.readUnsignedByte();
            this.mipmapLevel = stream.readUnsignedByte();
            this.pixels = null;
        } else {
            throw new DecodingException("Invalid sprite count of " + spriteCount);
        }
    }
}
