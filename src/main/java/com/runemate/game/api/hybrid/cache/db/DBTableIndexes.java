package com.runemate.game.api.hybrid.cache.db;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.concurrent.*;
import lombok.extern.log4j.*;

@Log4j2
public class DBTableIndexes {

    private static final DBTableIndexLoader LOADER = new DBTableIndexLoader();
    private static final Cache<Integer, DBTableIndex> CACHE = CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterAccess(5, TimeUnit.MINUTES)
        .build();

    public static DBTableIndex load(int index) {
        var def = CACHE.getIfPresent(index);
        if (def == null) {
            try {
                def = LOADER.load(JS5CacheController.getLargestJS5CacheController(), 0, index);
                if (def != null) {
                    CACHE.put(index, def);
                }
            } catch (IOException e) {
                log.warn("Failed to load DBTableIndex {}", index, e);
            }
        }
        return def;
    }

}
