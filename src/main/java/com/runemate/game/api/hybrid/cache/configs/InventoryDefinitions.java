package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import java.io.*;
import java.util.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@UtilityClass
public class InventoryDefinitions {

    private static final InventoryDefinitionLoader LOADER = new InventoryDefinitionLoader();

    public static InventoryDefinition load(int id) {
        try {
            var def = LOADER.load(ConfigType.INV, id);
            if (def != null) {
                return def;
            }
        } catch (IOException e) {
            log.warn("Unable to load InventoryDefinition with id {}", id, e);
        }
        return null;
    }

    public static List<InventoryDefinition> loadAll() {
        var quantity = LOADER.getFiles(ConfigType.INV).length;
        var results = new ArrayList<InventoryDefinition>(quantity);
        for (int id = 0; id <= quantity; id++) {
            var def = load(id);
            if (def != null) {
                results.add(def);
            }
        }
        results.trimToSize();
        return results;
    }
}
