package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.regex.*;
import javafx.scene.canvas.*;

public class InteractableShape implements Shape, Interactable, Renderable {
    private final Shape shape;

    public InteractableShape(final Shape shape) {
        if (shape == null) {
            throw new IllegalArgumentException("The shape argument cannot be null.");
        }
        this.shape = shape;
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public double getVisibility() {
        return 100;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        final Rectangle bounds = getBounds();
        if (bounds.width == 0 || bounds.height == 0) {
            return null;
        }
        InteractablePoint random = null;
        while (random == null || !bounds.contains(random)) {
            random = new InteractablePoint(
                Random.nextInt(bounds.x, bounds.x + bounds.width),
                Random.nextInt(bounds.y, bounds.y + bounds.height)
            );
        }
        return random;
    }

    @Override
    public boolean contains(Point point) {
        return shape.contains(point);
    }

    @Override
    public boolean contains(Rectangle2D rectangle) {
        return shape.contains(rectangle);
    }

    @Override
    public boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final String action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public boolean interact(Pattern action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(this, action, target);
    }

    @Override
    public boolean interact(Pattern action, String target) {
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return interact(action, targetPattern);
    }

    @Override
    public boolean interact(String action, Pattern target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        return interact(actionPattern, target);
    }

    @Override
    public String toString() {
        return "InteractableShape[" + shape + ']';
    }

    @Override
    public Rectangle getBounds() {
        return shape.getBounds();
    }

    @Override
    public Rectangle2D getBounds2D() {
        return shape.getBounds2D();
    }

    @Override
    public boolean contains(double x, double y) {
        return shape.contains(x, y);
    }

    @Override
    public boolean contains(Point2D p) {
        return shape.contains(p);
    }

    @Override
    public boolean intersects(double x, double y, double w, double h) {
        return shape.intersects(x, y, w, h);
    }

    @Override
    public boolean intersects(Rectangle2D r) {
        return shape.intersects(r);
    }

    @Override
    public boolean contains(double x, double y, double w, double h) {
        return shape.contains(x, y, w, h);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at) {
        return shape.getPathIterator(at);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return shape.getPathIterator(at, flatness);
    }

    @Override
    public void render(Graphics2D g2d) {
        Rectangle2D bounds = getBounds2D();
        g2d.drawRect((int) bounds.getX(), (int) bounds.getY(), (int) bounds.getWidth(),
            (int) bounds.getHeight()
        );
    }

    @Override
    public void render(GraphicsContext gc) {
        Rectangle2D bounds = getBounds2D();
        gc.strokeRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
    }
}
