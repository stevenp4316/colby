package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;

public abstract class RotatableQueryBuilder<T extends Rotatable & LocatableEntity, Q extends QueryBuilder>
    extends LocatableEntityQueryBuilder<T, Q> {
    private Locatable[] facing;

    public Q facing(final Locatable... facing) {
        this.facing = facing;
        return get();
    }

    @Override
    public boolean accepts(T argument) {
        boolean condition;
        if (facing != null) {
            condition = false;
            for (final Locatable value : facing) {
                if (argument.isFacing(value)) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }
}
