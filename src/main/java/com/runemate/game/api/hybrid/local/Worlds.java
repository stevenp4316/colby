package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

public final class Worlds {

    private Worlds() {
    }

    public static WorldQueryBuilder newQuery() {
        return new WorldQueryBuilder();
    }

    public static int getCurrent() {
        return OSRSWorldSelect.getCurrent();
    }

    public static int getPreferred() {
        final AbstractBot active = Environment.getBot();
        if (active == null) {
            return OpenClientPreferences.getPreferredDefaultWorld();
        }
        final Integer id = (Integer) active.getConfiguration().get("preferred.world");
        if (id == null) {
            return OpenClientPreferences.getPreferredDefaultWorld();
        }
        return id;
    }

    public static void setPreferred(int world) {
        final AbstractBot active = Environment.getBot();
        if (active != null) {
            active.getConfiguration().put("preferred.world", world);
        }
    }

    @Nullable
    public static WorldOverview getCurrentOverview() {
        return getOverview(getCurrent());
    }

    @Nullable
    public static WorldOverview getOverview(int world) {
        if (world == -1) {
            return null;
        }
        for (final WorldOverview w : getLoaded()) {
            if (w.getId() == world) {
                return w;
            }
        }
        return null;
    }

    public static WorldQueryResults getLoaded() {
        return getLoaded(null);
    }

    public static WorldQueryResults getLoaded(final Predicate<WorldOverview> filter) {
        return OSRSWorldSelect.getWorlds(filter);
    }
}
