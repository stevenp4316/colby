package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.net.*;
import java.util.*;
import java.util.concurrent.*;

public class GrandExchangeQueryResults
    extends QueryResults<GrandExchange.Slot, GrandExchangeQueryResults> {

    public GrandExchangeQueryResults(Collection<? extends GrandExchange.Slot> results) {
        super(results);
    }

    public GrandExchangeQueryResults(
        Collection<? extends GrandExchange.Slot> entries,
        ConcurrentMap<String, Object> cache
    ) {
        super(entries, cache);
    }

    @Override
    protected GrandExchangeQueryResults get() {
        return this;
    }
}
