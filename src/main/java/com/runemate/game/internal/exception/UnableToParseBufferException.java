package com.runemate.game.internal.exception;

public class UnableToParseBufferException extends RuntimeException {
    public UnableToParseBufferException(String explanation) {
        super(explanation);
    }

    public UnableToParseBufferException(String explanation, Exception cause) {
        super(explanation, cause);
    }
}
