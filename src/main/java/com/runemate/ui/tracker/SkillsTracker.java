package com.runemate.ui.tracker;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.concurrent.*;
import javafx.application.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SkillsTracker implements SkillListener {

    IntegerProperty totalExperienceGained = new SimpleIntegerProperty(0);
    IntegerProperty totalLevelsGained = new SimpleIntegerProperty(0);
    ObservableMap<Skill, SkillTracker> skills = FXCollections.observableMap(new ConcurrentHashMap<>());

    public SkillsTracker(@NonNull final AbstractBot bot) {
        bot.getEventDispatcher().addListener(this);
    }

    public SkillTracker getSkill(@NonNull Skill skill) {
        return skills.computeIfAbsent(skill, SkillTracker::new);
    }

    @Override
    public void onLevelUp(final SkillEvent event) {
        if (validate(event)) {
            update(getSkill(event.getSkill()).getExperienceGained(), event);
            update(getTotalLevelsGained(), event);
        }
    }

    @Override
    public void onExperienceGained(final SkillEvent event) {
        if (validate(event)) {
            final var skill = getSkill(event.getSkill());
            update(skill.getExperienceGained(), event);
            update(getTotalExperienceGained(), event);

            final var remaining = event.getSkill().getExperienceToNextLevel();
            final var progress = (double) event.getSkill().getExperienceAsPercent();
            Platform.runLater(() -> {
                skill.getLevel().setValue(Skills.getLevelAtExperience(event.getSkill(), event.getCurrent()));
                skill.getExperienceLeft().setValue(remaining);
                skill.getProgress().setValue(progress / 100d);
            });
        }
    }

    private boolean validate(SkillEvent event) {
        return event != null && event.getChange() > 0 && event.getPrevious() > 0;
    }

    private void update(@NonNull WritableNumberValue expr, @NonNull SkillEvent event) {
        final var current = expr.getValue().intValue();
        Platform.runLater(() -> expr.setValue(current + event.getChange()));
    }

    @Value
    public static class SkillTracker {

        Skill skill;
        IntegerProperty level = new SimpleIntegerProperty(0);
        DoubleProperty progress = new SimpleDoubleProperty(0);
        IntegerProperty experienceLeft = new SimpleIntegerProperty(0);
        IntegerProperty experienceGained = new SimpleIntegerProperty(0);
        IntegerProperty levelsGained = new SimpleIntegerProperty(0);
    }

}
