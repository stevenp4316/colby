package com.runemate.ui.control.setting;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.control.*;
import com.runemate.ui.converter.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.open.*;
import java.util.concurrent.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.apache.commons.text.*;

@Log4j2(topic = "Settings")
@UtilityClass
class Controls {

    Control build(AbstractBot bot, SettingsManager manager, Settings.Setting setting) {
        var type = setting.setting().type();
        if (type == boolean.class) {
            return Controls.forBoolean(manager, setting);
        }

        if (type == int.class) {
            return Controls.forInteger(manager, setting);
        }

        if (type == double.class) {
            return Controls.forDouble(manager, setting);
        }

        if (type == String.class) {
            return Controls.forString(manager, setting);
        }

        if (type == EquipmentLoadout.class) {
            return Controls.forEquipmentLoadout(bot.getPlatform(), manager, setting);
        }

        if (type == Coordinate.class) {
            return Controls.forCoordinate(bot.getPlatform(), manager, setting);
        }

        if (((Class<?>) type).isEnum()) {
            return Controls.forEnum(manager, setting);
        }

        log.warn("Unable to build setting control for type {}", setting.setting().type());
        return null;
    }

    Control forBoolean(SettingsManager manager, Settings.Setting setting) {
        var cb = defaults(manager, setting, new CheckBox());
        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), cb.selectedProperty(), mapper(setting, converter));
        cb.selectedProperty().addListener(new SettingChangeListener<>(setting, converter));
        return cb;
    }

    Control forInteger(SettingsManager manager, Settings.Setting setting) {
        var spinner = defaults(manager, setting, new Spinner<Integer>());
        var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);

        var range = setting.setting().getProperty(Range.class);
        if (range != null) {
            factory.setMin(range.min());
            factory.setMax(range.max());
            factory.setAmountToStepBy(range.step());
        }

        var suffix = setting.setting().getProperty(Suffix.class);
        if (suffix != null) {
            factory.setConverter(new SuffixConverter(suffix));
        }

        spinner.setEditable(true);
        spinner.setValueFactory(factory);

        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), factory.valueProperty(), mapper(setting, converter));
        factory.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return spinner;
    }

    Control forDouble(SettingsManager manager, Settings.Setting setting) {
        var spinner = defaults(manager, setting, new Spinner<Double>());
        var factory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, Double.MAX_VALUE);
        factory.setAmountToStepBy(0.1);
        spinner.setEditable(true);
        spinner.setValueFactory(factory);
        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), factory.valueProperty(), mapper(setting, converter));
        factory.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return spinner;
    }

    Control forString(SettingsManager manager, Settings.Setting setting) {
        TextInputControl control;
        if (setting.setting().setting().secret()) {
            control = new PasswordField();
        } else if (setting.setting().getProperty(Multiline.class) != null) {
            control = new TextArea();
            control.setPrefHeight(60);
        } else {
            control = new TextField();
        }
        control.setPrefWidth(120);

        defaults(manager, setting, control);
        control.setPromptText(setting.setting().setting().description());
        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), control.textProperty(), mapper(setting, converter));
        control.textProperty().addListener(new SettingChangeListener<>(setting, converter));
        return control;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    Control forEnum(SettingsManager manager, Settings.Setting setting) {
        var control = defaults(manager, setting, new ComboBox<Enum<?>>());
        var type = (Class<? extends Enum>) setting.setting().type();
        control.setMaxWidth(Double.MAX_VALUE);
        control.setItems(FXCollections.observableArrayList(type.getEnumConstants()));
        control.setConverter(new EnumConverter());

        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), control.valueProperty(), mapper(setting, converter));
        control.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return control;
    }

    Control forEquipmentLoadout(BotPlatform platform, SettingsManager manager, Settings.Setting setting) {
        var button = defaults(manager, setting, new Button("Equipment"));
        var property = new SimpleObjectProperty<>(new EquipmentLoadout());
        property.addListener((obs, old, loadout) -> button.setText(loadout.isEmpty() ? "Not set" : loadout.size() + " items"));

        button.setOnAction(event -> {
            button.setText("Loading...");
            var dialog = new EquipmentDialog(platform, property.get());
            dialog.showAndWait().ifPresentOrElse(property::set, () -> property.set(new EquipmentLoadout()));
        });

        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), property, mapper(setting, converter));
        property.addListener(new SettingChangeListener<>(setting, converter));
        return button;
    }

    Control forCoordinate(BotPlatform platform, SettingsManager manager, Settings.Setting setting) {
        var button = defaults(manager, setting, new Button("Not set"));
        var property = new SimpleObjectProperty<Coordinate>(null);
        property.addListener((obs, old, coord) -> button.setText(coord == null ? "Not set" : coord.toString()));

        button.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                property.set(null);
            }
        });

        button.setOnAction(event -> {
            button.setText("Loading...");
            try {
                var position = platform.invokeAndWait(() -> {
                    var player = Players.getLocal();
                    return player != null ? player.getPosition() : null;
                });
                property.set(position);
            } catch (ExecutionException | InterruptedException ignored) {
                log.warn("Failed to resolve player position for {}", setting.setting().key());
            }
        });

        var converter = manager.getConverter(setting.setting());
        Bindings.bindBidirectional(setting.settingProperty(), property, mapper(setting, converter));
        property.addListener(new SettingChangeListener<>(setting, converter));
        return button;
    }

    @RequiredArgsConstructor
    private static class SettingChangeListener<T> implements ChangeListener<T> {

        private final Settings.Setting setting;
        private final SettingConverter converter;

        @Override
        public void changed(final ObservableValue<? extends T> observable, final T oldValue, final T newValue) {
            setting.setSettingValue(converter.toString(newValue));
        }
    }

    private <T> StringConverter<T> mapper(Settings.Setting setting, SettingConverter converter) {
        return new StringConverter<>() {
            @Override
            public String toString(final T object) {
                return converter.toString(object);
            }

            @Override
            @SuppressWarnings("unchecked")
            public T fromString(final String string) {
                try {
                    return (T) converter.fromString(string, setting.setting().type());
                } catch (Exception e) {
                    log.warn("Error deserializing {}.{}", setting.group().group().group(), setting.setting().key(), e);
                    return null;
                }
            }
        };
    }

    private <T extends Control> T defaults(SettingsManager manager, Settings.Setting setting, T control) {
        control.setVisible(setting.shouldBeShown());
        control.setFocusTraversable(true);
        control.managedProperty().bind(control.visibleProperty());
        if (setting.setting().setting().disabled()) {
            control.setDisable(true);
        } else {
            control.disableProperty().bind(manager.lockedProperty());
        }

        return control;
    }

    private static class EnumConverter extends StringConverter<Enum<?>> {

        @Override
        public String toString(Enum<?> o) {
            if (o == null) {
                return null;
            }

            String toString = o.toString();
            //base toString() impl returns name()
            if (o.name().equals(toString)) {
                return WordUtils.capitalize(toString.toLowerCase(), '_').replace("_", " ");
            }

            return toString;
        }

        @Override
        public Enum<?> fromString(final String string) {
            return null;
        }
    }
}
